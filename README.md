Developed using javascript ES6 which supports the Class contruct.

Tests written using Cucumber-js with Frisby.js

Requires node.js to execute: download from https://nodejs.org/en/

Note: the API documentation does not describe how to create or update an employee's profile image.
The tests which create and assert the data has been stored correctly attempt to set this using the resource name "profile_image", but fail.

The tests which modify the salary and delete the employee record are successful.

1. Change to the project directory
2. Download the dependencies type: npm install 
3. Execute all tests type: npm run test  (json reports will be created in the reports directory)
4. Create pretty html reports type: npm run report
5. Click on Show info in report to expand and examine detailed information included request bod and responses
