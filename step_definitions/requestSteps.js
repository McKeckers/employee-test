var chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    expect = chai.expect;

chai.use(chaiAsPromised);

var { Given, When, Then, And, But } = require('cucumber');

const frisby = require('frisby');

var endPoint;
var result;

Given(/^the endpoint is (.*)$/, function (endpoint) {
    endPoint = endpoint;
});

Then(/^the response body should contain (.*)$/, function (text) {
    let json = this.response.json;
    this.attach(JSON.stringify(this.response.json, null, 2));
    expect(json).to.contain(text);
});

Then(/^the response status code should be (.*)$/, function (status) {
    expect(this.response._response.statusText).to.equal(status);
});

Then(/^the error message should contain (.*)$/, function (error) {
    expect(this.error).to.contain(error);
});


