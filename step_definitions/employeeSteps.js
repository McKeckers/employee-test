var chai = require('chai'),
  chaiAsPromised = require('chai-as-promised'),
  expect = chai.expect;

chai.use(chaiAsPromised);

var { Before, Given, When, Then } = require('cucumber');

const frisby = require('frisby');

const EmployeeAPI = require('../support/employeeAPI.js');
const Employee = require('../support/employee.js');
var employee;


async function deleteEmployee(id) {
  let deleteEmployee = EmployeeAPI.deleteEmployeeRoute(id);
  await frisby.del(deleteEmployee)
    .expect('status', 200)
    .then(function (deleteRes) {
      deleteJson = JSON.parse(deleteRes.body);
      console.log("Employee deleted");
    });
}

async function deleteIfExistsEmployee(name) {
  var employeeId;
  let getEmployeesRoute = EmployeeAPI.getEmployeesRoute();

  await frisby.get(getEmployeesRoute)
    .expect('status', 200)
    .then(function (getRes) { // res = FrisbyResponse object
      json = JSON.parse(getRes.body);

      for (i = 0; i < json.length; ++i) {
        let existingEmployee = json[i];
        if (existingEmployee.employee_name == employee.name) {
          console.log("Employee name exists");
          employeeId = existingEmployee.id;
          break;
        }
      }
    })

  if (employeeId != null) {
    await deleteEmployee(employeeId);
  }
}

async function createEmployee(world) {
  let body = employee.getBody();
  world.attach("Request body: " + JSON.stringify(body));

  let createEmployeeRoute = EmployeeAPI.createEmployeeRoute();
  await frisby.post(createEmployeeRoute, body)
    .expect('status', 200)
    .then((res) => {
      world.attach("Response: " + res._body);
      if (res._body.indexOf("error") >= 0) {
        error = res.body;
      } else {
        let json = JSON.parse(res.body);
        employee.id = json.id;
        console.log("Employee created: id: " + employee.id);
        error = "";
      }
      //set the world response
      world.response = res;
      world.error = error;
    });
}

When(/^the employee record (?:is|has been) created$/, async function () {
  // Need to check person doesn't already exist - if does then delete it first.
  await deleteIfExistsEmployee(employee.name);

  // now create it
  // pass in the CucumberWorld this so that we can set the response and error and check the result in a different step def class
  // If this was java or C# we could you Spring to autowire 1 step defn in another!
  await createEmployee(this);
});


When(/^the employee record is updated$/, async function () {
  let updateEmployeeRoute = EmployeeAPI.updateEmployeeRoute(employee.id);
  let body = employee.getBody();

  this.attach(JSON.stringify(body));
  await frisby.put(updateEmployeeRoute, body)
    .expect('status', 200)
    .then((res) => {
      this.attach("Response body: " + res.body);
      if (res._body.indexOf("error") >= 0) {
        error = res.body;
      } else {
        console.log("Employee created: id: " + employee.id);
        error = "";
      }
      //set the world response
      this.response = res;
      this.error = error;
    });

});


function logAndAssert(world, json) {
  // write to terminal for reassurance
  console.log("Employee retrieved: id: " + employee.id);
  console.log("Name: " + json.employee_name);
  console.log("Age: " + json.employee_age);
  console.log("Salary: " + json.employee_salary);
  console.log("Image: " + json.profile_image);

  // include in cucumber report
  world.attach("\nEmployee retrieved: id: " + employee.id);
  world.attach("Name: " + json.employee_name);
  world.attach("Age: " + json.employee_age);
  world.attach("Image: " + json.profile_image);

  // all these fields are mandatory
  expect(json.employee_name).to.equal(employee.name);
  expect(json.employee_salary).to.equal(employee.salary);
  expect(json.employee_age).to.equal(employee.age);

  if (employee.image!=null) {
    expect(json.profile_image).to.equal(employee.image);
  }
}

When(/^the data should be stored correctly$/, async function () {
  let getEmployeeRoute = EmployeeAPI.getEmployeeRoute(employee.id);

  await frisby.get(getEmployeeRoute)
    .expect('status', 200)
    .then((res) => {
      this.attach("Response: " + res._body);
      let json = JSON.parse(res.body);
      employee.id = json.id;
      logAndAssert(this, json);
      //this will still be CucumberWorld because used arrow function
      //set the world response
      this.response = res;
    });

});

When(/^the record should no longer exist$/, async function () {
  let getEmployeeRoute = EmployeeAPI.getEmployeeRoute(employee.id);

  await frisby.get(getEmployeeRoute)
    .expect('status', 200)
    .then((res) => {
      this.attach("Response: " + res._body);
      let json = JSON.parse(res.body);
      employee.id = json.id;
      expect(json).to.equal(false);
      //this will still be CucumberWorld because used arrow function
      //set the world response
      this.response = res;
    });

});

Given(/^an employee called (.*)$/, async function (name) {
  console.log("\nTest Scenario: " + this.scenario.pickle.name);
  employee = new Employee();
  employee.name = name;
});

Given(/^their age (?:is|was) (.*)$/, async function (age) {
  employee.age = age;
});

Given(/^an existing employee with salary (.*)$/, async function (salary) {
  console.log("\nTest Scenario: " + this.scenario.pickle.name);
  employee = new Employee();
  employee.name = "Jack Brown";
  employee.age = "35";
  employee.salary = salary;

  await deleteIfExistsEmployee(employee.name);
  await createEmployee(this);
});

When(/^the employee is deleted$/, async function () {
  await deleteEmployee(employee.id);
});

Given(/^their salary (?:is|changes to|was) (.*)$/, async function (salary) {
  employee.salary = salary;
});

Given(/^their image (?:is|was) (.*)$/, async function (image) {
  employee.image = image;
});