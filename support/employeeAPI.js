
var endpoint = 'http://dummy.restapiexample.com/api/v1';

class EmployeeAPI {

  static createEmployeeRoute() {
    let route = "/create";
    return endpoint + route;
  }

  static updateEmployeeRoute(id) {
    let route = "/update/";
    return endpoint + route + id;
  }

  static getEmployeeRoute(id) {
    let route = "/employee/";
    return endpoint + route + id;
  }

  static getEmployeesRoute() {
    let route = "/employees";
    return endpoint + route;
  }

  static deleteEmployeeRoute(id) {
    let route = "/delete/";
    return endpoint + route + id;
  }

}

module.exports = EmployeeAPI;

