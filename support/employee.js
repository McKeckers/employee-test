class Employee {

  constructor(name, age, salary, image) {
    this._name = name;
  }

  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  get age() {
    return this._age;
  }

  set age(age) {
    this._age = age;
  }

  get salary() {
    return this._salary;
  }

  set salary(salary) {
    this._salary = salary;
  }

  get image() {
    return this._image;
  }

  set image(image) {
    this._image = image;
  }

  get id() {
    return this._id;
  }

  set id(id) {
    this._id = id;
  }

  getBody() {
    // Note we have not been given the necessary resource name for creating the image
    return { "name": this._name, "age": this._age, "salary": this._salary, "profile_image": this._image };
  }

}

module.exports = Employee;

