var { After, AfterAll, Before } = require('cucumber');

Before(function (scenario) {
  this.scenario = scenario;
});


After(async function (scenario) {
  // Usually use thos for grabbing screen shote - irrelevant for API
  // Could use for closing db connections etc
});

