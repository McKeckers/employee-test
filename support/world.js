// features/support/world.js
const { setWorldConstructor } = require('cucumber')

class CustomWorld {
  constructor({attach, parameters}) {
    this.employee=null;
    this._currentCheckItem=null;
    this._response=null;
    this._scenario=null;
    this.attach = attach;
  }

  set employee(employee) {
    this._employee=employee;
  }
  
  get employee() {
    return this._employee;
  }

  set response(response) {
    this._response=response;
  }
  
  get response() {
    return this._response;
  }

  set error(error) {
    this._error=error;
  }
  
  get error() {
    return this._error;
  }

  set scenario(scenario) {
    this._scenario=scenario;
  }
  
  get scenario() {
    return this._scenario;
  }
  
}

setWorldConstructor(CustomWorld)




