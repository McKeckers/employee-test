var reporter = require('cucumber-html-reporter');
 
var options = {
    jsonFile: 'reports/cucumber-report.json',
    output: 'reports/cucumber_report.html',
    reportSuiteAsScenarios: true,
    launchReport: true,
    theme: "bootstrap",
    };
 
    reporter.generate(options);