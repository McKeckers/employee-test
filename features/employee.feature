@Employee
Feature: Employee

    @create
    Scenario Outline:  Create employee, test data stored correctly
        Note: profile image is not a mandatory field
        TODO: query how to create employee with image as not described in API documentation

        Given an employee called <name>
            And their age is <age>
            And their salary is <salary>
            And their image is <image>
        When the employee record is created
        Then the response status code should be OK
        Then the data should be stored correctly

        Examples:
            | name       | age | salary | image                  |
            | Joe Bloggs | 23  | 10000  | https://faces.com/1234 |
            | John Smith | 53  | 70000  | https://faces.com/3456 |

    @modify
    Scenario Outline:  Update salary, test data stored correctly

        Given an existing employee with salary <initial>
        When their salary changes to <new>
            And the employee record is updated
        Then the data should be stored correctly
        When the employee is deleted
        Then the record should no longer exist

        Examples:
            | initial | new   |
            | 10000   | 15000 |
            | 20000   | 25000 |

